package messagequeue

import "ProductPriceChanger/internal/domain"

type Producer interface {
	SendPriceUpdate(p domain.Product) error
}
