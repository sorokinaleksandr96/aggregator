INSERT INTO 
    data_fetcher.category (category_id, category_name) 
VALUES 
    ('750bf696-5bc2-4933-a1b6-47c9318a6f38', 'Лекарственные средства'),
    ('b9c3ee9e-6939-4bc7-bc8b-acc6ba3f9b8c', 'Одежда'),
    ('6cf5906e-0fe4-4d41-8f63-48e20ec8f32d', 'Бытовая техника'),
    ('36749e73-ad65-4859-bc25-83b7ce2cb8ef', 'Продукты питания');

INSERT INTO 
    data_fetcher.delivery_region (delivery_region_id, delivery_region_name) 
VALUES 
    ('2654af3e-86ae-4a03-9ef1-17edf7d1d2cf', 'Москва'),
    ('403bec00-aaf3-4cf6-8ed1-51450d78bfe3', 'Санкт-Петербург'),
    ('48401b13-9c9f-4e84-949c-3a6732c83d58', 'Новосибирск');

INSERT INTO 
    data_fetcher.product (product_id, product_name, category_id, product_price, product_manufacturer) 
VALUES 
    ('1a5976b9-b36a-433d-b596-7d04bb055bd6', 'Хлеб буханка',        '36749e73-ad65-4859-bc25-83b7ce2cb8ef', 32.0, 'Хлебзавод'),
    ('ba2b884a-a82f-463d-b2a6-792af0ff070e', 'Арбуз',               '36749e73-ad65-4859-bc25-83b7ce2cb8ef', 25.0, 'Агрохолдинг №7'),
    ('2e459e16-6948-4d46-b87b-2dec9cd5926c', 'Масло подсолнечное',  '36749e73-ad65-4859-bc25-83b7ce2cb8ef', 85.0, 'Агрохолдинг №7'),
    ('825d1897-2f36-43df-8f35-746570855175', 'Вино',                '36749e73-ad65-4859-bc25-83b7ce2cb8ef', 950.0, 'ООО Лучшие вина'),
    ('082362ff-0261-4f4f-ba51-542f92c38b37', 'Чай',                 '36749e73-ad65-4859-bc25-83b7ce2cb8ef', 370.0, 'Цейлон'),

    ('8f38c8e9-22b2-4ed4-a9ce-618a023db50e', 'Холодльник RFD-430I',             '6cf5906e-0fe4-4d41-8f63-48e20ec8f32d', 54992.0, 'Tesler'),
    ('20a6af63-4e99-46c8-affa-e486a1519bd2', 'Холодльник B419SQUL',             '6cf5906e-0fe4-4d41-8f63-48e20ec8f32d', 45419.0, 'LG'),
    ('44def5b7-2978-46eb-8f86-c9dcd4580e1c', 'Сплит-система BSD-09HN1',         '6cf5906e-0fe4-4d41-8f63-48e20ec8f32d', 24890.0, 'Ballu'),
    ('6699f652-0f67-4269-ac63-c241ed620f2e', 'Стиральная машина L 8WBE68 SRI',  '6cf5906e-0fe4-4d41-8f63-48e20ec8f32d', 159829.0, 'AEG'),
    ('6b37a58b-28fc-4071-a46e-38dd448aa323', 'Варочная панель HHE 6670 BG',     '6cf5906e-0fe4-4d41-8f63-48e20ec8f32d', 15750.0, 'Hyundai'),

    ('6bd6fc5d-49da-49f6-b66e-539c183c0358', 'Апровель',                        '750bf696-5bc2-4933-a1b6-47c9318a6f38', 892.0, 'SANOFI WINTHROP INDUSTRIE'),
    ('ea9302da-54dc-4134-98bf-b343884bff9e', 'Глазолин',                        '750bf696-5bc2-4933-a1b6-47c9318a6f38', 35.0, 'Polfa'),
    ('3d2fb32f-9c9a-4bcc-9905-77c6eb1326da', 'VPLab Ultra Mens Sport',          '750bf696-5bc2-4933-a1b6-47c9318a6f38', 999.0, 'VP Laboratory LTD'),
    ('943f9815-c0c8-499c-90f1-d6972665f7ca', 'VPLab Ultra Mens Sport',          '750bf696-5bc2-4933-a1b6-47c9318a6f38', 1925.0, 'VP Laboratory LTD'),

    ('8f30cda9-ddc4-4c7f-a17e-afc46eb89b4d', 'Куртка джинсовая',                'b9c3ee9e-6939-4bc7-bc8b-acc6ba3f9b8c', 4720, 'Levis'),
    ('c4979fd0-def6-4f58-8ee6-c2469303df70', 'Рубашка',                         'b9c3ee9e-6939-4bc7-bc8b-acc6ba3f9b8c', 3100, 'AI Franco'),
    ('9102018d-3bdc-4264-ad86-c11d4f98457d', 'Костюм классический Двойка',      'b9c3ee9e-6939-4bc7-bc8b-acc6ba3f9b8c', 15099, 'Stenser'),
    ('d75519d7-1388-45a3-a949-44429e984b02', 'Рюкзак CLSC XL',                  'b9c3ee9e-6939-4bc7-bc8b-acc6ba3f9b8c', 1840, 'Adidas'),
    ('bfeb05b6-e7d1-426c-ab79-be3694d92224', 'Брюки спортивные',                'b9c3ee9e-6939-4bc7-bc8b-acc6ba3f9b8c', 2990, 'GAP'),
    ('ad49eaa8-3d3c-49e0-b69b-780f306a5e48', 'Шорты',                           'b9c3ee9e-6939-4bc7-bc8b-acc6ba3f9b8c', 11299, 'Disel');

INSERT INTO 
    data_fetcher.product_x_propety (product_id, propety_name, propety_value) 
VALUES 
    ('2e459e16-6948-4d46-b87b-2dec9cd5926c', 'Объем', '1 л'),

    ('825d1897-2f36-43df-8f35-746570855175', 'Тип', 'Полусладкое'),
    ('825d1897-2f36-43df-8f35-746570855175', 'Крепокость', '12.5%'),

    ('082362ff-0261-4f4f-ba51-542f92c38b37', 'Сорт', 'Черный'),
    ('082362ff-0261-4f4f-ba51-542f92c38b37', 'Масса', '400 г'),

    ('8f38c8e9-22b2-4ed4-a9ce-618a023db50e', 'Цвет', 'Серый'),
    ('8f38c8e9-22b2-4ed4-a9ce-618a023db50e', 'Объем', '430 л'),

    ('20a6af63-4e99-46c8-affa-e486a1519bd2', 'Цвет', 'Белый'),
    ('20a6af63-4e99-46c8-affa-e486a1519bd2', 'Объем', '380 л'),

    ('44def5b7-2978-46eb-8f86-c9dcd4580e1c', 'Цвет', 'Белый'),
    ('44def5b7-2978-46eb-8f86-c9dcd4580e1c', 'Площадь помещения', '20 м2'),
    ('44def5b7-2978-46eb-8f86-c9dcd4580e1c', 'Уровень шума', '26 дб'),

    ('6699f652-0f67-4269-ac63-c241ed620f2e', 'Загрузка', '8 кг'),
    ('6699f652-0f67-4269-ac63-c241ed620f2e', 'Скорость отжима', '1600 об/мин'),

    ('6b37a58b-28fc-4071-a46e-38dd448aa323', 'Цвет', 'Черный'),

    ('6bd6fc5d-49da-49f6-b66e-539c183c0358', 'Дозировка', '15 мг'),
    ('6bd6fc5d-49da-49f6-b66e-539c183c0358', 'Число таблеток', '28'),

    ('ea9302da-54dc-4134-98bf-b343884bff9e', 'Тип', 'Капли'),
    ('ea9302da-54dc-4134-98bf-b343884bff9e', 'Дозировка', '0.1%'),
    ('ea9302da-54dc-4134-98bf-b343884bff9e', 'Объем', '10 мл'),

    ('3d2fb32f-9c9a-4bcc-9905-77c6eb1326da', 'Число таблеток', '90'),
    ('943f9815-c0c8-499c-90f1-d6972665f7ca', 'Число таблеток', '180'),

    ('8f30cda9-ddc4-4c7f-a17e-afc46eb89b4d', 'Размер', 'M'),
    ('8f30cda9-ddc4-4c7f-a17e-afc46eb89b4d', 'Цвет', 'Синий'),
    ('8f30cda9-ddc4-4c7f-a17e-afc46eb89b4d', 'Сезон', 'Лето'),

    ('c4979fd0-def6-4f58-8ee6-c2469303df70', 'Размер', 'S'),
    ('c4979fd0-def6-4f58-8ee6-c2469303df70', 'Цвет', 'Белый'),
    ('c4979fd0-def6-4f58-8ee6-c2469303df70', 'Материал', 'Хлопок'),

    ('9102018d-3bdc-4264-ad86-c11d4f98457d', 'Размер', '56'),
    ('9102018d-3bdc-4264-ad86-c11d4f98457d', 'Цвет', 'Черный'),

    ('d75519d7-1388-45a3-a949-44429e984b02', 'Цвет', 'Сервый'),
    ('d75519d7-1388-45a3-a949-44429e984b02', 'Объем', '40 л'),

    ('bfeb05b6-e7d1-426c-ab79-be3694d92224', 'Размер', 'S'),
    ('bfeb05b6-e7d1-426c-ab79-be3694d92224', 'Цвет', 'Бежевый'),
    ('bfeb05b6-e7d1-426c-ab79-be3694d92224', 'Материал', 'Хлопок'),

    ('ad49eaa8-3d3c-49e0-b69b-780f306a5e48', 'Размер', 'M'),
    ('ad49eaa8-3d3c-49e0-b69b-780f306a5e48', 'Цвет', 'Синий');

INSERT INTO
    data_fetcher.product_x_delivery_region (product_id, delivery_region_id)
VALUES
    ('1a5976b9-b36a-433d-b596-7d04bb055bd6', '48401b13-9c9f-4e84-949c-3a6732c83d58'),

    ('ba2b884a-a82f-463d-b2a6-792af0ff070e', '2654af3e-86ae-4a03-9ef1-17edf7d1d2cf'),
    ('ba2b884a-a82f-463d-b2a6-792af0ff070e', '403bec00-aaf3-4cf6-8ed1-51450d78bfe3'),

    ('2e459e16-6948-4d46-b87b-2dec9cd5926c', '2654af3e-86ae-4a03-9ef1-17edf7d1d2cf'),
    ('2e459e16-6948-4d46-b87b-2dec9cd5926c', '403bec00-aaf3-4cf6-8ed1-51450d78bfe3'),

    ('825d1897-2f36-43df-8f35-746570855175', '2654af3e-86ae-4a03-9ef1-17edf7d1d2cf'),

    ('082362ff-0261-4f4f-ba51-542f92c38b37', '2654af3e-86ae-4a03-9ef1-17edf7d1d2cf'),
    ('082362ff-0261-4f4f-ba51-542f92c38b37', '403bec00-aaf3-4cf6-8ed1-51450d78bfe3'),
    ('082362ff-0261-4f4f-ba51-542f92c38b37', '48401b13-9c9f-4e84-949c-3a6732c83d58');

WITH products as (
    SELECT
        product_id
    FROM
        data_fetcher.product
    WHERE
        category_id in (
            '6cf5906e-0fe4-4d41-8f63-48e20ec8f32d',
            '750bf696-5bc2-4933-a1b6-47c9318a6f38',
            'b9c3ee9e-6939-4bc7-bc8b-acc6ba3f9b8c'
        )
)

INSERT INTO
    data_fetcher.product_x_delivery_region (product_id, delivery_region_id)
SELECT
    p.product_id,
    d.delivery_region_id
FROM
    products AS p
    CROSS JOIN data_fetcher.delivery_region as d;

DELETE
FROM
    data_fetcher.product_x_delivery_region
WHERE
    product_id = 'b9c3ee9e-6939-4bc7-bc8b-acc6ba3f9b8c'
    or (
        product_id = '6b37a58b-28fc-4071-a46e-38dd448aa323'
        and delivery_region_id != '48401b13-9c9f-4e84-949c-3a6732c83d58'
    )
    or (
        product_id in (
            '3d2fb32f-9c9a-4bcc-9905-77c6eb1326da',
            '943f9815-c0c8-499c-90f1-d6972665f7ca'
        )
        and delivery_region_id != '403bec00-aaf3-4cf6-8ed1-51450d78bfe3'
    );





