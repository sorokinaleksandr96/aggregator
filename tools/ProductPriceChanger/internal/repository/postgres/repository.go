package postgres

import (
	"database/sql"
	"fmt"

	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"

	"ProductPriceChanger/internal/domain"
)

const (
	productTableName = "product"
)

type Config struct {
	User   string
	Pass   string
	DB     string
	Host   string
	Port   string
	Schema string
}

func (c Config) ConnectURL() string {
	return fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s?sslmode=disable",
		c.User, c.Pass, c.Host, c.Port, c.DB,
	)
}

type Repository struct {
	db           *sqlx.DB
	schemaPrefix string
}

func NewRepository(c Config) (*Repository, error) {
	db, err := sqlx.Connect("pgx", c.ConnectURL())
	if err != nil {
		return nil, fmt.Errorf("failed to connect to db: %w", err)
	}

	schemaPrefix := ""
	if c.Schema != "" {
		schemaPrefix = c.Schema + "."
	}
	return &Repository{
		db:           db,
		schemaPrefix: schemaPrefix,
	}, nil
}

func (r *Repository) GetSomeProducts(count int) ([]domain.Product, error) {
	query := fmt.Sprintf(`
	SELECT
		product_id,
		product_price
	FROM
		%s%s
	ORDER BY
		random()
	limit $1
	`, r.schemaPrefix, productTableName)

	products := []domain.Product{}
	if err := r.db.Select(&products, query, count); err != nil {
		return nil, fmt.Errorf("failed to get products: %w", err)
	}

	return products, nil
}

func (r *Repository) UpdateProduct(p domain.Product) (*sql.Tx, error) {
	query := fmt.Sprintf(`
	UPDATE
		%s%s
	SET
		product_price = $1
	WHERE
		product_id = $2
	`, r.schemaPrefix, productTableName)

	tx, err := r.db.Begin()
	if err != nil {
		return nil, fmt.Errorf("failed to begin tx: %w", err)
	}

	if _, err := tx.Exec(query, p.Price, p.ID); err != nil {
		tx.Rollback()
		return nil, fmt.Errorf("failed to update product price %w", err)
	}

	return tx, nil
}

func (r *Repository) Close() {
	r.db.Close()
}
