package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/Shopify/sarama"
	"golang.org/x/sync/errgroup"

	"DataFetcher/internal/app"
	"DataFetcher/internal/repository/mongo"
	"DataFetcher/internal/repository/postgres"
	"DataFetcher/pkg/runner"
	"DataFetcher/pkg/simpleconsumer"
)

var (
	configFileName string
)

func main() {
	flag.StringVar(&configFileName, "config", "./configs/app.yml", "path to yaml config file")
	flag.Parse()

	config, err := app.ConfigFromYaml(configFileName)
	if err != nil {
		fmt.Printf("failed to read config file: %v\n", err)
		return
	}

	pgRepo, err := postgres.NewRepository(config.Postgres)
	if err != nil {
		fmt.Printf("failed to create postgres external repo: %v\n", err)
		return
	}
	defer pgRepo.Close()

	mongoRepo, err := mongo.NewRepository(context.TODO(), config.Mongo)
	if err != nil {
		fmt.Printf("failed to create mongo internal repo: %v\n", err)
		return
	}
	defer mongoRepo.Close(context.TODO())

	saramaConfig := sarama.NewConfig()
	consumer, err := simpleconsumer.New(
		saramaConfig, config.Kafka.Brokers, config.Kafka.Topics, config.Kafka.ConsumerGroup,
	)
	if err != nil {
		fmt.Printf("failed to create kafka simpleconsumer: %v\n", err)
		return
	}
	defer consumer.Close()

	migrateFunc := app.GetMirgrateFunc(pgRepo, mongoRepo, 3)
	priceUpdateFunc := app.GetPriceUpdateHandlerFunc(mongoRepo)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	eg, egctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		fmt.Println("price updater started")
		err := consumer.ListenAndServe(egctx, priceUpdateFunc)
		return fmt.Errorf("consumer terminated with errror: %w", err)
	})
	eg.Go(func() error {
		fmt.Println("migrator started")
		err := runner.RepeatableRunner(egctx, migrateFunc, 5*time.Minute)
		return fmt.Errorf("runner terminated with errror: %w", err)
	})

	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM)

	select {
	case sig := <-exit:
		fmt.Printf("recived %v, terminating\n", sig)
		cancel()
		eg.Wait()
	case <-egctx.Done():
		err := eg.Wait()
		fmt.Printf("%v, terminating", err.Error())
	}
}
