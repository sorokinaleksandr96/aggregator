package config

import (
	"errors"
	"os"

	"ProductPriceChanger/internal/messagequeue/kafka"
	"ProductPriceChanger/internal/repository/postgres"
)

type Config struct {
	Postgres postgres.Config
	Kafka    kafka.Config
}

func NewFromEnv() (Config, error) {
	c := Config{}

	c.Postgres.User = os.Getenv("POSTGRES_USER")
	if c.Postgres.User == "" {
		return c, errors.New("POSTGRES_USER is missed")
	}

	c.Postgres.Pass = os.Getenv("POSTGRES_PASSWORD")
	if c.Postgres.Pass == "" {
		return c, errors.New("POSTGRES_PASSWORD is missed")
	}

	c.Postgres.DB = os.Getenv("POSTGRES_DB")
	if c.Postgres.DB == "" {
		return c, errors.New("POSTGRES_DB is missed")
	}

	c.Postgres.Host = os.Getenv("POSTGRES_HOST")
	if c.Postgres.Host == "" {
		return c, errors.New("POSTGRES_HOST is missed")
	}

	c.Postgres.Port = os.Getenv("POSTGRES_PORT")
	if c.Postgres.Port == "" {
		c.Postgres.Port = "5432"
	}

	if schema, exists := os.LookupEnv("POSTGRES_SCHEMA"); exists {
		c.Postgres.Schema = schema
	} else {
		return c, errors.New("POSTGRES_SCHEMA is missed")
	}

	kafkaBroker := os.Getenv("KAFKA_BROKER")
	if kafkaBroker == "" {
		return c, errors.New("KAFKA_BROKER is missed")
	} else {
		c.Kafka.Brokers = []string{kafkaBroker}
	}

	c.Kafka.Topic = os.Getenv("KAFKA_TOPIC")
	if c.Kafka.Topic == "" {
		return c, errors.New("KAFKA_TOPIC is missed")
	}
	return c, nil
}
