package pricechanger

import (
	"fmt"
	"math/rand"

	"ProductPriceChanger/internal/domain"
	"ProductPriceChanger/internal/messagequeue"
	"ProductPriceChanger/internal/repository"
)

func getNewPrice(oldPrice uint, changePercentLimit float64) uint {
	changePercent := (2*rand.Float64() - 1) * changePercentLimit
	return uint((1.0 + changePercent) * float64(oldPrice))
}

type PriceChanger struct {
	producer messagequeue.Producer
	updater  repository.ProductUpdater

	changePercentLimit float64
}

func New(p messagequeue.Producer, r repository.ProductUpdater, changePercentLimit float64) *PriceChanger {
	return &PriceChanger{
		producer:           p,
		updater:            r,
		changePercentLimit: changePercentLimit,
	}
}

func (pr *PriceChanger) ChangePrice(p domain.Product) (int, error) {
	newPrice := getNewPrice(p.Price, pr.changePercentLimit)
	p.Price = newPrice

	tx, err := pr.updater.UpdateProduct(p)
	if err != nil {
		return 0, fmt.Errorf("failed to update: %w", err)
	}

	if err := pr.producer.SendPriceUpdate(p); err != nil {
		tx.Rollback()
		return 0, fmt.Errorf("failed to send message: %w", err)
	}

	if err := tx.Commit(); err != nil {
		return 0, fmt.Errorf("failed to commit transaction: %w", err)
	}
	return int(newPrice), nil
}
