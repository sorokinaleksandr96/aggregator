package simpleconsumer

import (
	"context"
	"errors"

	"github.com/Shopify/sarama"
)

type HandlerFunc func(context.Context, *sarama.ConsumerMessage) error

type consumer struct {
	hf HandlerFunc
}

func newConsumer(hf HandlerFunc) (*consumer, error) {
	if hf == nil {
		return nil, errors.New("handlefunc should be non nil")
	}

	return &consumer{
		hf: hf,
	}, nil
}

func (c *consumer) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (c *consumer) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (c *consumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	// NOTE:
	// Do not move the code below to a goroutine.
	// The `ConsumeClaim` itself is called within a goroutine, see:
	// https://github.com/Shopify/sarama/blob/main/consumer_group.go#L27-L29
	for {
		select {
		case message := <-claim.Messages():
			go func() {
				err := c.hf(session.Context(), message)
				if err == nil {
					session.MarkMessage(message, "")
				}
			}()

		// Should return when `session.Context()` is done.
		// If not, will raise `ErrRebalanceInProgress` or `read tcp <ip>:<port>: i/o timeout` when kafka rebalance. see:
		// https://github.com/Shopify/sarama/issues/1192
		case <-session.Context().Done():
			return session.Context().Err()
		}
	}
}
