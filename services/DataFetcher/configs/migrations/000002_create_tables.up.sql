CREATE TABLE data_fetcher.category (
    category_id     UUID PRIMARY KEY,
    category_name   TEXT NOT NULL
);

CREATE TABLE data_fetcher.delivery_region (
    delivery_region_id      UUID PRIMARY KEY,
    delivery_region_name    TEXT NOT NULL
);

CREATE TABLE data_fetcher.product (
    id                      SERIAL,
    product_id              UUID PRIMARY KEY,
    product_name            TEXT NOT NULL,
    category_id             UUID,
    product_price           INT NOT NULL,
    product_manufacturer    TEXT NOT NULL,

    FOREIGN KEY (category_id)
        REFERENCES data_fetcher.category (category_id)
            ON DELETE RESTRICT
);

CREATE UNIQUE INDEX product_ididx ON data_fetcher.product (id);

CREATE TABLE data_fetcher.product_x_propety (
    product_id      UUID,
    propety_name    TEXT NOT NULL,
    propety_value   TEXT NOT NULL,

    PRIMARY KEY (product_id, propety_name),
    FOREIGN KEY (product_id)
        REFERENCES data_fetcher.product (product_id)
            ON DELETE CASCADE
);

CREATE TABLE data_fetcher.product_x_delivery_region (
    product_id          UUID,
    delivery_region_id  UUID,

    PRIMARY KEY (product_id, delivery_region_id),

    FOREIGN KEY (product_id)
        REFERENCES data_fetcher.product (product_id)
            ON DELETE CASCADE,

    FOREIGN KEY (delivery_region_id)
        REFERENCES data_fetcher.delivery_region (delivery_region_id)
            ON DELETE CASCADE
);