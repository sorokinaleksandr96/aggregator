package app

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/Shopify/sarama"

	"DataFetcher/internal/repository"
	"DataFetcher/pkg/simpleconsumer"
)

type priceUpdate struct {
	ProductID string `json:"id"`
	Price     int    `json:"price"`
}

func GetPriceUpdateHandlerFunc(updater repository.PriceUpdater) simpleconsumer.HandlerFunc {
	return func(ctx context.Context, cm *sarama.ConsumerMessage) error {
		var update priceUpdate
		err := json.Unmarshal(cm.Value, &update)
		if err != nil {
			return fmt.Errorf("failed to parse json from message: %w", err)
		}

		tctx, cancel := context.WithTimeout(ctx, 3*time.Second)
		defer cancel()

		err = updater.UpdateProductPrice(tctx, update.ProductID, update.Price)
		if err != nil {
			return fmt.Errorf("failed to update price: %w", err)
		}

		fmt.Printf("Partition:%d\tOffset:%d\t%s\n", cm.Partition, cm.Offset, string(cm.Value))
		return nil
	}
}
