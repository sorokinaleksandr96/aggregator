package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgtype"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"

	"DataFetcher/internal/domain"
)

const (
	productTableName                = "product"
	categoryTableName               = "category"
	productPropetyTableName         = "product_x_propety"
	productDeliveryRegionsTableName = "product_x_delivery_region"
	deliveryRegionTableName         = "delivery_region"
)

type Config struct {
	User   string
	Pass   string
	DB     string
	Host   string
	Port   string
	Schema string
}

func (c Config) ConnectURL() string {
	return fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s?sslmode=disable",
		c.User, c.Pass, c.Host, c.Port, c.DB,
	)
}

type Repository struct {
	db     *sqlx.DB
	schema string
}

func NewRepository(c Config) (*Repository, error) {
	db, err := sqlx.Connect("pgx", c.ConnectURL())
	if err != nil {
		return nil, fmt.Errorf("failed to connect to db: %w", err)
	}

	return &Repository{db: db, schema: c.Schema}, nil
}

func GenerateProductQuery(schema string, limit int, offset int) string {
	var offsetStr string
	if offset > 0 {
		offsetStr = fmt.Sprintf(`WHERE 
			p.id > %d`, offset)
	}

	var limitStr string
	if limit > 0 {
		limitStr = fmt.Sprintf(`LIMIT
			%d`, limit)
	}

	var schemaStr string
	if schema != "" {
		schemaStr = schema + "."
	}
	return fmt.Sprintf(`
		SELECT
			p.product_id,
			p.product_name,
			c.category_name,
			p.product_price,
			p.product_manufacturer
		FROM
			%s%s AS p
			JOIN %s%s AS c
				ON p.category_id = c.category_id
		%s
		%s`,
		schemaStr, productTableName,
		schemaStr, categoryTableName,
		offsetStr,
		limitStr,
	)
}

func getProductInfo(ctx context.Context, db *sqlx.DB, schema string, num int, offset int) ([]domain.Product, error) {
	query := GenerateProductQuery(schema, num, offset)

	var ps []domain.Product
	if err := db.SelectContext(ctx, &ps, query); err != nil {
		return nil, fmt.Errorf("failed to select product info: %w", err)
	}

	return ps, nil
}

func generateProducPropetiesQuery(schema string) string {
	var schemaStr string
	if schema != "" {
		schemaStr = schema + "."
	}

	return fmt.Sprintf(`
		SELECT
			product_id,
			propety_name,
			propety_value
		FROM
			%s%s
		WHERE
			product_id = ANY($1)
	`, schemaStr, productPropetyTableName)
}

func getProductPropeties(ctx context.Context, db *sqlx.DB, schema string, ids []string) (map[string][]domain.ProductProperty, error) {
	query := generateProducPropetiesQuery(schema)
	rows, err := db.QueryContext(ctx, query, ids)
	if err != nil {
		return nil, fmt.Errorf("failed to select property data: %w", err)
	}
	defer rows.Close()

	id, name, value := "", "", ""
	m := map[string][]domain.ProductProperty{}
	for rows.Next() {
		err = rows.Scan(&id, &name, &value)
		if err != nil {
			return nil, fmt.Errorf("failed to scan property data: %w", err)
		}

		m[id] = append(m[id], domain.ProductProperty{Name: name, Value: value})
	}

	return m, nil
}

func generateRegionsQuery(schema string) string {
	var schemaStr string
	if schema != "" {
		schemaStr = schema + "."
	}

	return fmt.Sprintf(`
		SELECT
			pxr.product_id,
			array_agg(r.delivery_region_name) as delivery_regions
		FROM
			%s%s AS pxr
			JOIN %s%s AS r
				ON pxr.delivery_region_id = r.delivery_region_id
		WHERE
			pxr.product_id = ANY($1)
		GROUP BY
			pxr.product_id
	`, schemaStr, productDeliveryRegionsTableName, schemaStr, deliveryRegionTableName)
}

func getRegions(ctx context.Context, db *sqlx.DB, schema string, ids []string) (map[string][]string, error) {
	query := generateRegionsQuery(schema)
	rows, err := db.QueryContext(ctx, query, ids)
	if err != nil {
		return nil, fmt.Errorf("failed to select delivery regions: %w", err)
	}
	defer rows.Close()

	id, regions := "", pgtype.TextArray{}
	m := map[string][]string{}
	for rows.Next() {
		err = rows.Scan(&id, &regions)
		if err != nil {
			return nil, fmt.Errorf("failed to scan delivery regions data: %w", err)
		}

		s := []string{}
		err = regions.AssignTo(&s)
		if err != nil {
			return nil, fmt.Errorf("failed to assing []text to []string: %w", err)
		}
		m[id] = s
	}

	return m, nil
}

func (r *Repository) GetProductBatch(ctx context.Context, num int, offset int) ([]domain.Product, error) {
	ps, err := getProductInfo(ctx, r.db, r.schema, num, offset)
	if err != nil {
		return nil, fmt.Errorf("failed to get product info: %w", err)
	}

	productIDs := []string{}
	for _, p := range ps {
		productIDs = append(productIDs, p.ID)
	}
	productPropeties, err := getProductPropeties(ctx, r.db, r.schema, productIDs)
	if err != nil {
		return nil, fmt.Errorf("failed to get product propeties: %w", err)
	}

	regionsMap, err := getRegions(ctx, r.db, r.schema, productIDs)
	if err != nil {
		return nil, fmt.Errorf("failed to get product delivery regions: %w", err)
	}

	for i, p := range ps {
		ps[i].DeliveryRegions = regionsMap[p.ID]
		ps[i].Properties = productPropeties[p.ID]

		if len(ps[i].DeliveryRegions) == 0 {
			ps[i].DeliveryRegions = []string{}
		}
		if len(ps[i].Properties) == 0 {
			ps[i].Properties = []domain.ProductProperty{}
		}

	}

	return ps, nil
}

func (r *Repository) GetAllProducts(ctx context.Context) ([]domain.Product, error) {
	return r.GetProductBatch(ctx, -1, -1)
}

func getProductCount(ctx context.Context, db *sqlx.DB, schema string) (int, error) {
	var schemaStr string
	if schema != "" {
		schemaStr = schema + "."
	}

	query := fmt.Sprintf(`
	SELECT
		MAX(id)
	FROM
		%s%s
	`, schemaStr, productTableName)

	count := 0
	if err := db.Select(&count, query); err != nil {
		return 0, fmt.Errorf("failed to select product count: %w", err)
	}

	return count, nil
}

func (r *Repository) GetProductCount(ctx context.Context) (int, error) {
	return getProductCount(ctx, r.db, r.schema)
}

func (r *Repository) Close() {
	r.db.Close()
}
