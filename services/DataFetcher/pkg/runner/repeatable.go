package runner

import (
	"context"
	"fmt"
	"time"
)

type fu func(ctx context.Context) error

// Run f at start and every "interval" after until ctx expire
func RepeatableRunner(ctx context.Context, f fu, interval time.Duration) error {
	doneCh := make(chan error, 1)
	go func() {
		doneCh <- f(ctx)
	}()

	var timer <-chan time.Time
	for {
		select {
		case err := <-doneCh:
			if err != nil {
				return fmt.Errorf("f finished with error: %w", err)
			}
			timer = time.After(interval)
		case <-timer:
			go func() {
				doneCh <- f(ctx)
			}()
		case <-ctx.Done():
			return ctx.Err()
		}
	}
}
