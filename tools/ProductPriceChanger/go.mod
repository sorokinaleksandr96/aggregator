module ProductPriceChanger

go 1.16

require (
	github.com/Shopify/sarama v1.34.1
	github.com/jackc/pgx/v4 v4.16.1
	github.com/jmoiron/sqlx v1.3.5
)
