package simpleconsumer

import (
	"context"
	"fmt"

	"github.com/Shopify/sarama"
)

type KafkaConfig struct {
	Brokers       []string
	Topics        []string
	ConsumerGroup string
}

type SimpleConsumer struct {
	client sarama.ConsumerGroup

	topics []string
}

func New(сonfig *sarama.Config, brokers []string, topics []string, group string) (*SimpleConsumer, error) {
	client, err := sarama.NewConsumerGroup(brokers, "g2", sarama.NewConfig())
	if err != nil {
		return nil, fmt.Errorf("failed to create consumer group: %w", err)
	}

	return &SimpleConsumer{
		client: client,
		topics: topics,
	}, nil
}

func (c *SimpleConsumer) ListenAndServe(ctx context.Context, hf HandlerFunc) error {
	consumer, err := newConsumer(hf)
	if err != nil {
		return fmt.Errorf("failed to create consumer: %w", err)
	}

	for {
		err := c.client.Consume(ctx, c.topics, consumer)
		if err != nil {
			return fmt.Errorf("error from consumer: %w", err)
		}
		fmt.Println("client got away from consume")
		if ctx.Err() != nil {
			return ctx.Err()
		}
	}
}

func (c *SimpleConsumer) Close() error {
	return c.client.Close()
}
