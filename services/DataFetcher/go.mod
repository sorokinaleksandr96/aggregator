module DataFetcher

go 1.16

require (
	github.com/Shopify/sarama v1.34.1 // indirect
	github.com/Shopify/toxiproxy v2.1.4+incompatible // indirect
	github.com/frankban/quicktest v1.4.1 // indirect
	github.com/jackc/pgtype v1.11.0 // indirect
	github.com/jackc/pgx v3.6.2+incompatible // indirect
	github.com/jackc/pgx/v4 v4.16.1 // indirect
	github.com/jmoiron/sqlx v1.3.5 // indirect
	github.com/pierrec/lz4 v2.2.6+incompatible // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.9.1 // indirect
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f // indirect
	gopkg.in/jcmturner/aescts.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/dnsutils.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/goidentity.v3 v3.0.0 // indirect
	gopkg.in/jcmturner/gokrb5.v7 v7.2.3 // indirect
	gopkg.in/jcmturner/rpc.v1 v1.1.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
