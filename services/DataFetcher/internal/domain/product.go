package domain

type ProductProperty struct {
	Name  string `bson:"name"`
	Value string `bson:"value"`
}

type Product struct {
	ID           string `db:"product_id" bson:"id"`
	Name         string `db:"product_name" bson:"name"`
	Category     string `db:"category_name" bson:"category"`
	Price        int    `db:"product_price" bson:"price"`
	Manufacturer string `db:"product_manufacturer" bson:"manufacturer"`

	Properties      []ProductProperty `bson:"properties"`
	DeliveryRegions []string          `bson:"deliveryRegions"`
}

type PriceChange struct {
	ProductID string
	NewPrice  int
}
