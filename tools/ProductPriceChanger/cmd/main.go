package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"time"

	"ProductPriceChanger/internal/app/config"
	"ProductPriceChanger/internal/app/pricechanger"
	"ProductPriceChanger/internal/messagequeue/kafka"
	"ProductPriceChanger/internal/repository"
	"ProductPriceChanger/internal/repository/postgres"
)

// Commandline arguments
var (
	changePercentLimit float64
	productNum         int
)

func main() {
	flag.Float64Var(&changePercentLimit, "cl", 0.2, "maximum possible price change ratio")
	flag.IntVar(&productNum, "n", 2, "number of products to change price")
	flag.Parse()

	retcode := 0
	defer func() { os.Exit(retcode) }()

	config, err := config.NewFromEnv()
	if err != nil {
		fmt.Printf("failed to get config from env: %v\n", err)
		return
	}

	postgres, err := postgres.NewRepository(config.Postgres)
	if err != nil {
		fmt.Printf("failed to create postgres repository: %v\n", err)
		retcode = -1
		return
	}
	defer postgres.Close()

	kafka, err := kafka.NewProducer(config.Kafka)
	if err != nil {
		fmt.Printf("failed to create kafka producer: %v\n", err)
		retcode = -1
		return
	}
	defer kafka.Close()

	productGetter := repository.ProductGetter(postgres)
	products, err := productGetter.GetSomeProducts(productNum)
	if err != nil {
		fmt.Printf("failed to get data from postgres: %v\n", err)
		retcode = -1
		return
	}

	rand.Seed(time.Now().Unix())
	priceChanger := pricechanger.New(kafka, postgres, changePercentLimit)

	for _, p := range products {
		newPrice, err := priceChanger.ChangePrice(p)
		if err != nil {
			fmt.Printf("failed to change product price: %v\n", err)
		} else {
			fmt.Printf("%s price changed: %d -> %d\n", p.ID, p.Price, newPrice)
		}
	}
}
