package repository

import (
	"database/sql"

	"ProductPriceChanger/internal/domain"
)

type ProductGetter interface {
	GetSomeProducts(count int) ([]domain.Product, error)
}

type ProductUpdater interface {
	UpdateProduct(p domain.Product) (*sql.Tx, error)
}
