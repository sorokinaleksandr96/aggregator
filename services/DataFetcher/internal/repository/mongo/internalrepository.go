package mongo

import (
	"context"
	"errors"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"DataFetcher/internal/domain"
)

const (
	productCollectionName        = "product"
	productCollectionVersionName = "collectionVersion"
)

var (
	emptyFilter = bson.D{{}}
)

type Config struct {
	User string
	Pass string
	Host string
	Port string
	DB   string
}

func (c Config) ConnectURL() string {
	if c.User != "" && c.Pass != "" {
		return fmt.Sprintf("mongodb://%s:%s@%s:%s", c.User, c.Pass, c.Host, c.Port)
	}
	return fmt.Sprintf("mongodb://%s:%s", c.Host, c.Port)
}

type Repository struct {
	db *mongo.Database
}

func NewRepository(ctx context.Context, c Config) (*Repository, error) {
	clientOptions := options.Client()
	clientOptions.ApplyURI(c.ConnectURL())
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(c.ConnectURL()))
	if err != nil {
		return nil, fmt.Errorf("failed to create client: %w", err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		return nil, fmt.Errorf("cannot connect to mongo: %w", err)
	}

	db := client.Database(c.DB, nil)

	return &Repository{
		db: db,
	}, nil
}

func (r *Repository) Close(ctx context.Context) error {
	return r.db.Client().Disconnect(ctx)
}

func (r *Repository) GetVersion(ctx context.Context) (int, error) {
	//filter := bson.D{{}}
	sr := r.db.Collection(productCollectionVersionName).FindOne(ctx, emptyFilter)
	if sr.Err() != nil {
		return 0, fmt.Errorf("failed to get collection for version: %w", sr.Err())
	}

	type version struct {
		Version int `bson:"version"`
	}
	v := version{}
	if err := sr.Decode(&v); err != nil {
		return 0, fmt.Errorf("failed to decode bson: %w", err)
	}

	return v.Version, nil
}

func (r *Repository) UpdateVersion(ctx context.Context) error {
	//filter := bson.D{{}}
	update := bson.D{{"$inc", bson.D{{"version", 1}}}}
	ur, err := r.db.Collection(productCollectionVersionName).UpdateOne(ctx, emptyFilter, update)
	if err != nil {
		return fmt.Errorf("failed to get collection for version: %w", err)
	}

	if ur.ModifiedCount != 1 {
		return errors.New("collection for version is broken: recived multiple update")
	}

	return nil
}

func productToVersionedBsonM(p *domain.Product, version int) bson.M {
	return bson.M{
		"version": version,
		"product": *p,
	}
}

func (r *Repository) UpsertProducts(ctx context.Context, ps []domain.Product, version int) error {
	models := make([]mongo.WriteModel, len(ps))
	for i := range ps {
		m := mongo.NewUpdateOneModel()
		m.SetFilter(bson.D{{"product.id", ps[i].ID}})
		m.SetUpdate(bson.D{{"$set", productToVersionedBsonM(&ps[i], version)}})
		m.SetUpsert(true)
		models[i] = m
	}

	opts := options.BulkWrite().SetOrdered(false)
	if _, err := r.db.Collection(productCollectionName).BulkWrite(ctx, models, opts); err != nil {
		return fmt.Errorf("failed to perform collection update: %w", err)
	}

	return nil
}

func (r *Repository) RemoveOldProducts(ctx context.Context, version int) error {
	filter := bson.D{{"version", bson.D{{"$lt", version}}}}

	if _, err := r.db.Collection(productCollectionName).DeleteMany(ctx, filter); err != nil {
		return fmt.Errorf("failed to delete elements from product collection: %w", err)
	}

	return nil
}

func (r *Repository) UpdateProductPrice(ctx context.Context, ID string, newPrice int) error {
	filter := bson.D{{"product.id", ID}}
	update := bson.D{{"$set", bson.D{{"product.price", newPrice}}}}

	if _, err := r.db.Collection(productCollectionName).UpdateOne(ctx, filter, update); err != nil {
		return fmt.Errorf("failed to update product price: %w", err)
	}

	return nil
}
