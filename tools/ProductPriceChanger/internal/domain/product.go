package domain

type Product struct {
	ID    string `db:"product_id"`
	Price uint   `db:"product_price"`
}
