package app

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"

	"DataFetcher/internal/repository/mongo"
	"DataFetcher/internal/repository/postgres"
	"DataFetcher/pkg/simpleconsumer"
)

type Config struct {
	Postgres postgres.Config
	Mongo    mongo.Config
	Kafka    simpleconsumer.KafkaConfig
}

func ConfigFromYaml(path string) (Config, error) {
	c := Config{}

	file, err := os.ReadFile(path)
	if err != nil {
		return c, fmt.Errorf("failed to read config file: %w", err)
	}

	err = yaml.Unmarshal(file, &c)
	if err != nil {
		return c, fmt.Errorf("failed to parse yaml file into config: %w", err)
	}

	return c, nil
}
