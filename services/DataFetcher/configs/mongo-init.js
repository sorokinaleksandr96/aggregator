db.createCollection(
    "product", {
    "validator": {
        $jsonSchema: {
            "bsonType": "object",
            "required": ["product", "version"],
            "additionalProperties": false,
            "properties": {
                "_id": {},
                "product": {
                    "bsonType": "object",
                    "required": ["id", "name", "category", "price", "manufacturer"],
                    "additionalProperties": false,
                    "properties": {
                        "id": {
                            "bsonType": "string",
                        },
                        "name": {
                            "bsonType": "string",
                        },
                        "category": {
                            "bsonType": "string",
                        },
                        "price": {
                            "bsonType": "int",
                        },
                        "manufacturer": {
                            "bsonType": "string",
                        },
                        "properties": {
                            "bsonType": "array",
                            "items": {
                                "bsonType": "object",
                                "required": ["name", "value"],
                                "additionalProperties": false,
                                "properties": {
                                    "name": {
                                        "bsonType": "string",
                                    },
                                    "value": {
                                        "bsonType": "string",
                                    },
                                }
                            }
                        },
                        "deliveryRegions": {
                            "bsonType": "array",
                            "items": {
                                "bsonType": "string"
                            }
                        }
                    }
                },
                "version": {
                    "bsonType": "int"
                }
            }
        }
    }
});

db.product.createIndex( { "product.id": 1 }, { unique: true } );

db.createCollection(
    "collectionVersion", {
        "validator": {
            $jsonSchema: {
                "bsonType": "object",
                "required": ["version"],
                "additionalProperties": false,
                "properties": {
                    "_id": {},
                    "version": {
                        "bsonType": "int"
                    }
                }
            }
        }
    }
);

db.collectionVersion.insertOne({"version": NumberInt(0)});