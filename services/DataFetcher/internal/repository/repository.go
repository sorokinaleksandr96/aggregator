package repository

import (
	"context"

	"DataFetcher/internal/domain"
)

type ExternalRepository interface {
	GetAllProducts(ctx context.Context) ([]domain.Product, error)

	// This or generator-like?
	GetProductCount(ctx context.Context) (int, error)
	GetProductBatch(ctx context.Context, num int, offset int) ([]domain.Product, error)
}

type VersionedInternalRepository interface {
	// Get current data version in repository
	GetVersion(ctx context.Context) (int, error)
	// Increment data version
	UpdateVersion(ctx context.Context) error

	// Upsert products by ID and marks them with provided version
	UpsertProducts(ctx context.Context, p []domain.Product, version int) error
	// Remove all products with smaller versions
	RemoveOldProducts(ctx context.Context, version int) error
}

type PriceUpdater interface {
	UpdateProductPrice(ctx context.Context, ID string, newPrice int) error
}
