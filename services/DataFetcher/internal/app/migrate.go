package app

import (
	"context"
	"fmt"

	"DataFetcher/internal/repository"
)

func GetMirgrateFunc(
	er repository.ExternalRepository, ir repository.VersionedInternalRepository, batchSize int,
) func(ctx context.Context) error {
	return func(ctx context.Context) error {
		err := ir.UpdateVersion(ctx)
		if err != nil {
			return fmt.Errorf("failed to update repository version: %w", err)
		}
		version, err := ir.GetVersion(ctx)
		if err != nil {
			return fmt.Errorf("failed to get repository version: %w", err)
		}

		offset := 0
		for {
			ps, err := er.GetProductBatch(ctx, batchSize, offset)
			if err != nil {
				return fmt.Errorf("failed to get product batch: %w", err)
			}

			err = ir.UpsertProducts(ctx, ps, version)
			if err != nil {
				return fmt.Errorf("failed to upsert product batch: %w", err)
			}

			if batchSize <= 0 || len(ps) < batchSize {
				break
			} else {
				offset += len(ps)
			}
		}

		ir.RemoveOldProducts(ctx, version)
		return nil
	}
}
