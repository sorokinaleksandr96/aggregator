package kafka

import (
	"encoding/json"
	"fmt"

	"github.com/Shopify/sarama"

	"ProductPriceChanger/internal/domain"
)

type Config struct {
	Brokers []string
	Topic   string
}

type Producer struct {
	producer sarama.SyncProducer
	topic    string
}

func NewProducer(c Config) (*Producer, error) {
	producer, err := sarama.NewSyncProducer(c.Brokers, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create producer: %w", err)
	}

	return &Producer{
		producer: producer,
		topic:    c.Topic,
	}, nil
}

func (producer *Producer) SendPriceUpdate(p domain.Product) error {
	jsn, err := json.Marshal(p)
	if err != nil {
		return fmt.Errorf("failed to marshal message: %w", err)
	}

	msg := &sarama.ProducerMessage{
		Topic: producer.topic,
		Value: sarama.ByteEncoder(jsn),
	}

	if _, _, err := producer.producer.SendMessage(msg); err != nil {
		return fmt.Errorf("failed to send message to kafka: %w", err)
	}

	return err
}

func (producer *Producer) Close() {
	producer.producer.Close()
}
